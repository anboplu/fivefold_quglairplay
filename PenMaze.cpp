#include "ffShapeDrawer.h"
#include "ffShapeDrawer.h"
#include "substitute.h"
#include "PenMaze.h"
#include "QuGlobals.h"
#include "QuCamera.h"

bool bShowAll = false;
bool bShowExit = true;
bool bPathLight = false;
bool bReachLight = true;
bool bWaveLight = true;
bool bHighlight = false;



FiveCoord& operator += (FiveCoord& f1, FiveCoord f2)
{
    for(int i = 0; i < 5; ++i)
        f1.v[i] += f2.v[i];
    return f1;
}

FiveCoord operator + (FiveCoord f1, FiveCoord f2)
{
    FiveCoord t = f1;
    t += f2;
    return t;
}
bool SameSide(fPoint p1, fPoint p2, fPoint l1, fPoint l2)
{
    fPoint p = p2 - p1;
    fPoint l = l2 - l1;

    float a1 = p.y;
    float b1 = -p.x;
    float c1 = p1.x*a1 + p1.y*b1;
    float a2 = l.y;
    float b2 = -l.x;
    float c2 = l1.x*a2 + l1.y*b2;

    if(abs((b1*a2- b2*a1)) < .0001F)
        return true;

    float y =  ( c1*a2 - c2*a1 ) / ( b1*a2- b2*a1 );
    float x = -( c1*b2 - c2*b1 ) / ( b1*a2- b2*a1 );

    fPoint c(x, y);

    if( (c - p1).Length() < p.Length() )
        if( (c - p2).Length() < p.Length() )
            return false;
    return true;
}

std::ostream& operator << (std::ostream& ostr, FiveCoord f)
{
    for(int i = 0; i < 5; ++i)
        ostr << f.v[i] << " ";
    return ostr;
}

std::istream& operator >> (std::istream& istr, FiveCoord& f)
{
    for(int i = 0; i < 5; ++i)
        istr >> f.v[i];
    return istr;
}

int FiveCoord::GetSum() const
{
    return v[0] + v[1] + v[2] + v[3] + v[4];
}


fPoint GetDir(double d)
{
    return fPoint(float(cos(HALF_PI + d)), float(-sin(HALF_PI + d)));
}

fPoint GetDir(int n)
{
    return GetDir(TWO_PI/5 * n);
}

float Dst(fPoint f1, fPoint f2)
{
    return (f1 - f2).Length();
}

Pentagon::Pentagon(FiveCoord fc_, float fRad_, bool bFlipped_, WallDef vWalls_)
    :fc(fc_), fRad(fRad_), bFlipped(bFlipped_), vWalls(vWalls_), nAge(0)
{
    fCenter = fPoint();
    int i;
    for(i = 0; i < 5; ++i)
        fCenter += GetDir(i)*float(fc[i])*fRad*2*cos(TWO_PI/10);
    for(i = 0; i < 5; ++i)
        vVert.push_back(fCenter + GetDir(i)*fRad * float(bFlipped ? -1 : 1) );
    for(i = 0; i < 5; ++i)
    {
        Side sd;
        sd.p1 = vVert[i];
        sd.p2 = vVert[(i + 1)%5];
        sd.fcDelta[(i + 3)%5] = (bFlipped ? 1 : -1);
        vSides.push_back(sd);
    }
}

Pentagon SimplePenta(FiveCoord fc)
{
    return Pentagon(fc, 1.F, (fc.GetSum() == 0) ? false : true, WallDef());
}

bool Pentagon::CloseToVertice(fPoint p)
{
    int i;
    for(i = 0; i < 5; ++i)
        if(Dst(p, vVert[i]) < (fRad / 10))
            return true;
    return false;
}

bool Pentagon::CrossingWall(fPoint p, fPoint& pWall)
{
    int i;
    for(i = 0; i < 5; ++i)
        if(vWalls[i] && !SameSide(fCenter, p, vSides[i].p1, vSides[i].p2))
		{
            pWall = vSides[i].p2 - vSides[i].p1;
			return true;
		}
    return false;
}


FiveCoord Pentagon::CrossSide(fPoint p)
{
    int i;
    for(i = 0; i < 5; ++i)
        if(!SameSide(fCenter, p, vSides[i].p1, vSides[i].p2))
            return vSides[i].fcDelta;
    return FiveCoord();
}

bool Intersect(Pentagon& pent, fPoint p)
{
    for(int i = 0; i < 5; ++i)
        if(!SameSide(pent.fCenter, p, pent.vSides[i].p1, pent.vSides[i].p2))
            return false;
    return true;
}


bool Intersect(Pentagon& pent1, Pentagon& pent2)
{
    FiveCoord fcDiff = pent1.fc - pent2.fc;

    int nC = 0;

    for(int j = 0; j < 5; ++j)
        if(fcDiff[j] == 1 || fcDiff[j] == -1)
            ++nC;
        else if (fcDiff[j] != 0)
            nC += 10;
    if(nC == 1 || nC == 2)
        return false;
    
    for(int i = 0; i < 5; ++i)
        if(Intersect(pent1, pent2.vVert[i]))
            return true;
    return false;
}

void Game::DrawPentagon(Pentagon& p, int nPath, int nFrame)
{
    drawPentagonPulse(!p.bFlipped, p.fCenter.x, p.fCenter.y,
                        DrawingParameters(p.lsPulse, nPath, p.nAge, 1.F * nCountDown / 100),
                        nFrame, p.vWalls.v, p.fRad, p.fc == fcExit);
}
void Game::Draw(float fZoom)
{
    if(nCountDown != 100 && nCountDown > 0)
    {
        nCountDown -= 5;
        if(nCountDown <= 0)
            bFinished = true;
    }
    
	QuBasicCamera cam(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT);
	cam.setRotation(FOLD_SCREEN_ROTATION);
	pOffset = fPoint(cam.getWidth()/2, cam.getHeight()/2);
    glPushMatrix();
	cam.setScene();

	//dont need this since my camera is already centered in the middle
	//glTranslatef(pOffset.x, pOffset.y,0);

    glScalef(fZoom, fZoom,1);

	if(bMenu)
	{
		glPushMatrix();
		//TODO this should only happen when not in the menu
		glTranslatef(-pGuy.x, pGuy.y,0);
		drawMenuBackground();
		glPopMatrix();
	}

    glPushMatrix();
	glScalef(1,-1,1);
	glTranslatef(-pGuy.x, -pGuy.y,0);

	int n = G_GAME_GLOBAL.getFrameCount();

    if(!bMenu && bShowExit)
        DrawPentagon(*mpLevel[fcExit], 100, n);

    if(bShowAll)
        for(set<Pentagon*>::iterator itr = stOther.begin(), etr = stOther.end(); itr != etr; ++itr)
            DrawPentagon(**itr, 100, n);

    int i = lsVisible.size() - 1;
    for(list<Pentagon*>::iterator itr = lsVisible.begin(), etr = lsVisible.end(); itr != etr; ++itr, --i)
        DrawPentagon(**itr, i, n);

    drawGuy(pGuy.x, pGuy.y, n);

	glPopMatrix();
	glPopMatrix();
}
fPoint Project(fPoint pDir, fPoint pWall)
{
	float fDot = pDir.x * pWall.x + pDir.y * pWall.y;
	fDot /= pWall.x*pWall.x + pWall.y*pWall.y;
	return pWall * fDot;
}

void Game::Move(fPoint fDir)
{
    fDir.Normalize(fSpeed);

    fPoint pNew = pGuy + fDir;

    Pentagon& p = *lsVisible.back();

    fPoint pWall;
	
	if(p.CrossingWall(pNew, pWall))
	{
		fDir = Project(fDir, pWall);

		pNew = pGuy + fDir;

		if(p.CrossingWall(pNew, pWall))
			return;
	}


    FiveCoord fc = p.CrossSide(pNew);
    if(!(fc == FiveCoord()))
    {
        if(mpLevel.find(p.fc + fc) == mpLevel.end())
            return;

        Move(p.fc + fc);
    }

    pGuy = pNew;
}
void Game::Move(FiveCoord fc)
{
    bool bExit = false;
    int i = lsVisible.size() - 1;
    for(list<Pentagon*>::iterator itr = lsVisible.begin(), etr = lsVisible.end(); itr != etr; ++itr, --i)
        if((*itr)->fc == fc)
        {
            lsVisible.push_back(*itr);
            lsVisible.erase(itr);
            bExit = true;
            break;
        }

    if(!bExit)
    {
        map<FiveCoord, Pentagon*>::iterator itr = mpLevel.find(fc);

        if(itr == mpLevel.end())
            return;

        lsVisible.push_back(itr->second);
        stOther.erase(itr->second);
    }
    
    MarkDistances();

    if(bReachLight)
    {
        std::list<Pentagon*> lsPathCopy;
        std::list<Pentagon*> lsPathAppend;
        
        for(map<FiveCoord, Pentagon*>::iterator itr = mpLevel.begin(), etr = mpLevel.end(); itr != etr; ++itr)
        {
            set<Pentagon*>::iterator sitr = stOther.find(itr->second);

            if(sitr == stOther.end())
                continue;
            
            if(itr->second->nAge <= 1)
            {
                lsPathAppend.push_back(itr->second);
                stOther.erase(sitr);
            }
        }

        for(list<Pentagon*>::iterator itr = lsVisible.begin(), etr = lsVisible.end(); itr != etr; ++itr)
        {
            if((*itr)->nAge <= 1)
                lsPathAppend.push_back(*itr);
            else
                lsPathCopy.push_back(*itr);
        }
        
        lsVisible = lsPathCopy;

        for(list<Pentagon*>::iterator itr = lsPathAppend.begin(), etr = lsPathAppend.end(); itr != etr; ++itr)
            lsVisible.push_back(*itr);
    }

    if(!bMenu)
    {
        if(fc == fcExit)
            --nCountDown;
    }
    else
    {
        if(!(fc == FiveCoord(0,0,0,0,0)))
        {
            for(int i = 0; i < 5; ++i)
                if(fc[i] != 0)
                    nStatus = i;

            --nCountDown;
        }
    }
}

void Game::MarkDistances()
{
    map<FiveCoord, Pentagon*> mpPath;
    for(map<FiveCoord, Pentagon*>::iterator itr = mpLevel.begin(), etr = mpLevel.end(); itr != etr; ++itr)
    {
        mpPath[itr->first] = itr->second;
        itr->second->nAge = -1;
    }

    lsVisible.back()->nAge = 0;

    list<FiveCoord> lsRecursive;
    lsRecursive.push_back(lsVisible.back()->fc);

    while(!lsRecursive.empty())
    {
        FiveCoord fc = lsRecursive.front();
        lsRecursive.pop_front();
        Pentagon* pP = mpPath[fc];
        for(int i = 0; i < 5; ++i)
        {
            if(pP->vWalls[i])
                continue;
            Side s = pP->vSides[i];
            FiveCoord fcNew = s.fcDelta + fc;
            
            map<FiveCoord, Pentagon*>::iterator itr = mpPath.find(fcNew);
            if(itr != mpPath.end() && itr->second->nAge == -1)
            {
                itr->second->nAge = pP->nAge + 1;
                lsRecursive.push_back(fcNew);
            }
        }
    }
}

void Game::Pulse()
{
    for(list<Pentagon*>::iterator itr = lsVisible.begin(), etr = lsVisible.end(); itr != etr; ++itr)
    {
        Pentagon& p = **itr;
        p.lsPulse.push_back(ffShapeDrawerPair(G_GAME_GLOBAL.getFrameCount(), p.nAge));
        if(p.lsPulse.size() > 10)
            p.lsPulse.pop_front();
    }

    if(!bShowAll)
        return;

    for(set<Pentagon*>::iterator itr = stOther.begin(), etr = stOther.end(); itr != etr; ++itr)
    {
        Pentagon& p = **itr;
        p.lsPulse.push_back(ffShapeDrawerPair(G_GAME_GLOBAL.getFrameCount(), p.nAge));
        if(p.lsPulse.size() > 10)
            p.lsPulse.pop_front();
    }
}



Game::Game(bool bMenu_)
:bFinished(false), nCountDown(100), bMenu(bMenu_), nStatus(-1)
{        
    pGuy = fPoint(0,0);
    fSpeed = 5;
}


Spot* PentaMaze::GetSpot(FiveCoord fc)
{
    std::map<FiveCoord, Spot*>::iterator itr =
        mp.find(fc);
    if(itr == mp.end())
        return 0;
    return itr->second;
}

FiveCoord RandomGrid2D(int nL, bool bReg)
{
    FiveCoord fRet(nL,nL,nL,nL,nL);
    
    int n = rand()%5;
    fRet[n] = 0;
    int nM = (rand()%2 == 0) ? -1 : 1;
    n = n + 5 + nM * ( bReg ? 2 : 1);
    n %= 5;
    fRet[n] = 0;

    return fRet;
}

FiveCoord RandomGrid3D(int nL)
{
    FiveCoord fRet(nL,nL,nL,nL,nL);
    fRet[rand()%5] = 0;
    return fRet;
}

PentaMaze::PentaMaze(FiveCoord fcBnd_, FiveCoord fcWeight_, bool bFillOut) 
:fcWeight(fcWeight_), fcBnd(fcBnd_)
{

    if(bFillOut)
    {
        FiveCoord fc;
        for(fc[0] = -fcBnd[0]; fc[0] <= fcBnd[0]; ++fc[0])
        for(fc[1] = -fcBnd[1]; fc[1] <= fcBnd[1]; ++fc[1])
        for(fc[2] = -fcBnd[2]; fc[2] <= fcBnd[2]; ++fc[2])
        for(fc[3] = -fcBnd[3]; fc[3] <= fcBnd[3]; ++fc[3])
        for(fc[4] = -fcBnd[4]; fc[4] <= fcBnd[4]; ++fc[4])
        {
            if((fc.GetSum() == 0 || fc.GetSum() == -1) && (GetSpot(fc) == 0))
            {
                Spot* pS = new Spot();
                pS->vLinks.resize(5);
                mp[fc] = pS;
            }
        }

        cout << "Maze size: " << mp.size() << "\n";
    }
    else
    {
        Spot* pS = new Spot();
        pS->vLinks.resize(5);
        mp[FiveCoord(0,0,0,0,0)] = pS;
    }
    
    PassAssignment();
}

void PentaMaze::PassAssignment()
{
    for(unsigned i = 0; i < v.size(); ++i)
        delete v[i];

    v.clear();

    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
        SpotPassAssignment(itr->first, itr->second);
}

void PentaMaze::SpotPassAssignment(FiveCoord fc, Spot* pS)
{
    for(int i = 0; i < 5; ++i)
    {
        FiveCoord fc_new = fc;
        if(fc.GetSum() == 0)
            --fc_new[i];
        else
            ++fc_new[i];
        Spot* pNew = GetSpot(fc_new);
        if(pNew)
        {
            pS->vLinks[i].pTo = pNew;
            pNew->vLinks[i].pTo = pS;

            Pass* pPass = new Pass(true, fcWeight[i]);
            v.push_back(pPass);
            
            pS->vLinks[i].pPass = pPass;
            pNew->vLinks[i].pPass = pPass;
        }
    }
}


void PentaMaze::PartialMaze()
{
    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr;)
    {
        if(itr->second->nState != 0)
            mp.erase(itr++);
        else
            itr++;
    }

    PassAssignment();
}



void PentaMaze::PlaceEndpoints(bool bRand)
{
    Maze mz;
    GetMaze(mz);

    SpotPair sp;
    if(!bRand)
        sp = MaxDistance(mz, mz);
    else
    {
        Maze exMaze;
        exMaze.vSpots.push_back(mz.vSpots[rand()%mz.vSpots.size()]);
        sp = MaxDistance(mz, exMaze);
    }

    mz.Flush(0);
    sp.p1->nState = 1;
    sp.p2->nState = 2;

    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
    {
        if(itr->second->nState == 1)
            fcEnter = itr->first;
        if(itr->second->nState == 2)
            fcExit = itr->first;
    }

    mz.Flush(0);
}


void PentaMaze::GetMaze(Maze& mz)
{
    mz.vSpots.clear();
    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
        mz.vSpots.push_back(itr->second);
}


PentaMaze::~PentaMaze()
{
    for(unsigned i = 0; i < v.size(); ++i)
        delete v[i];
    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
        delete itr->second;
}

void PentaMaze::InitLevel(std::map<FiveCoord, WallDef>& mpLevel)
{
    mpLevel.clear();
    for(std::map<FiveCoord, Spot*>::iterator itr = mp.begin(), etr = mp.end(); itr != etr; ++itr)
    {
        Spot* pS = itr->second;
        vector<bool> vWall(5, true);
        for(int i = 0; i < 5; ++i)
        {
            int n = (i+3)%5;
            if(pS->vLinks[n].pPass && !pS->vLinks[n].pPass->bWalled)
                vWall[i] = false;
        }
        mpLevel[itr->first] = vWall;
    }
}

bool PentaMaze::OutOfBound(FiveCoord fc)
{
    for(int i = 0; i < 5; ++i)
        if(-fcBnd[i] > fc[i] || fc[i] > fcBnd[i])
            return true;
    return false;
}

void PentaMazeFrontier::Clear()
{
    std::vector<Spot*> v;

    for(map<Spot*, FiveCoord>::iterator itr = mpFr.begin(), etr = mpFr.end(); itr != etr; ++itr)
        v.push_back(itr->first);

    mpFr.clear();

    for(unsigned i = 0; i < v.size(); ++i)
        delete v[i];
}



void PentaMazeRestrict(PentaMaze& pm, std::vector<Spot*>& vFrontiers)
{
    std::vector<FiveCoord> vBoundaryMaze;
    std::vector<FiveCoord> vCarvedMaze;
    std::set<Spot*> stRestrictedSpots;
    std::set<Pass*> stRestrictedPasses;

    for(std::map<FiveCoord, Spot*>::iterator itr = pm.mp.begin(), etr = pm.mp.end(); itr != etr; ++itr)
        if(!IsIsolated(itr->second))
            vCarvedMaze.push_back(itr->first);
        else
        {
            Spot* pSpot = itr->second;
            unsigned n;
            for(n = 0; n < pSpot->vLinks.size(); ++n)
                if(!IsIsolated(pSpot->vLinks[n].pTo))
                    break;

            if(n != pSpot->vLinks.size())
                vBoundaryMaze.push_back(itr->first);
        }

    std::cout << vBoundaryMaze.size() << " " << vCarvedMaze.size() << " " << pm.mp.size() << "\n";

    for(unsigned i = 0; i < vBoundaryMaze.size(); ++i)
    for(unsigned j = 0; j < vCarvedMaze.size(); ++j)
	{
		Pentagon p1 = SimplePenta(vBoundaryMaze[i]);
		Pentagon p2 = SimplePenta(vCarvedMaze[j]);
        if(Intersect(p1, p2))
        {
            Spot* pSpot = pm.mp[vBoundaryMaze[i]];
            stRestrictedSpots.insert(pSpot);
            for(unsigned n = 0; n < pSpot->vLinks.size(); ++n)
                stRestrictedPasses.insert(pSpot->vLinks[n].pPass);
        }
	}

    stRestrictedPasses.erase(0);

    for(std::map<FiveCoord, Spot*>::iterator itr = pm.mp.begin(), etr = pm.mp.end(); itr != etr; ++itr)
    {
        Spot* pSpot = itr->second;
        for(unsigned n = 0; n < pSpot->vLinks.size(); ++n)
            if(stRestrictedPasses.find(pSpot->vLinks[n].pPass) != stRestrictedPasses.end())
            {
                pSpot->vLinks[n].pPass = 0;
                pSpot->vLinks[n].pTo = 0;
            }
    }

    std::vector<Spot*> vFrontiersNew;
    for(unsigned i = 0; i < vFrontiers.size(); ++i)
        if(stRestrictedSpots.find(vFrontiers[i]) == stRestrictedSpots.end())
            vFrontiersNew.push_back(vFrontiers[i]);

    vFrontiers = vFrontiersNew;

    for(std::map<FiveCoord, Spot*>::iterator itr = pm.mp.begin(), etr = pm.mp.end(); itr != etr;)
        if(stRestrictedSpots.find(itr->second) != stRestrictedSpots.end())
        {
            Spot* pSpot = itr->second;
            pm.mp.erase(itr++);
            delete pSpot;
        }
        else
            ++itr;
}

void AdjustPentaFrontiers(PentaMaze* pPmz, PentaMazeFrontier& pmf, Spot* pSpot, Maze& mz, int nClump)
{
    std::map<Spot*, FiveCoord>::iterator itr = pmf.mpFr.find(pSpot);
    
    if(itr == pmf.mpFr.end())
        return;

    FiveCoord fc = itr->second;
    
    pmf.mpFr.erase(itr);

    //pPmz->mp[fc] = pSpot;

    for(int i = 0; i < 5; ++i)
    {
        FiveCoord fc_new = fc;
        if(fc.GetSum() == 0)
            --fc_new[i];
        else
            ++fc_new[i];
        
        if(pPmz->GetSpot(fc_new))
            continue;

        if(pPmz->OutOfBound(fc_new))
            continue;

        if(nClump != -1)
        {
            int nIntersect = 0;
            for(std::map<FiveCoord, Spot*>::iterator itr = pPmz->mp.begin(), etr = pPmz->mp.end(); itr != etr; ++itr)
            {
				
				Pentagon p1 = SimplePenta(fc_new);
				Pentagon p2 = SimplePenta(itr->first);
                if(Intersect(p1, p2))
                {
                    ++nIntersect;
                    if(nIntersect >= nClump)
                        break;
                }
            }

            if(nIntersect >= nClump)
                continue;
        }
            
            
        Spot* pNew = new Spot();
        pNew->vLinks.resize(5);

        pPmz->mp[fc_new] = pNew;
        pmf.mpFr[pNew] = fc_new;
        mz.vSpots.push_back(pNew);

        pPmz->SpotPassAssignment(fc_new, pNew);
    }
}
#include "ffShapeDrawer.h"
#include <vector>
#include <cmath>
#include <iostream>
#include "QuDrawer.h"
#include "QuGlobals.h"
#include "QuCamera.h"
#include "substitute.h"

extern bool bShowAll;
extern bool bShowExit;
extern bool bPathLight;
extern bool bReachLight;
extern bool bWaveLight;
extern bool bHighlight;


void drawLoadingScreen(float percentdividedbyonehundredasafloat)
{

}
void drawPentagon(bool isFlipped, float _x, float _y, vector<bool> drawSides,  float _iter, float radius, int sides)
{
	if(drawSides.size() != 5)
		drawSides = vector<bool> (5, true);

    double offset = 0;
	if(isFlipped)
		offset = PI;	//should be pi
	
	//fade is overloaded, -1 is default value and draws pentagon at it's lightest visible setting
	//(0,1) is used for continuous fade amounts
	//0,1,2,3... are used for iterative fading

	Qu32BitColor color;
	color.a = 170 - _iter*16;
	if(_iter == -1 || color.a < 32)
		color.a = 32;
	if(_iter > 0 && _iter < 1)
	{
		color.a = 255*_iter - 64;
		
	}

	//determine pentagon points
	std::vector<ffShapeDrawerPair> pts = std::vector<ffShapeDrawerPair>(sides);
	for(int i = 0; i < sides; i++)
		pts[i] = ffShapeDrawerPair(radius*sin(offset+(PI*2*i/(float)sides)),radius*cos(offset+(PI*2*i/(float)sides)));


	//transform to the position we want
	glPushMatrix();
	glTranslatef(_x,_y,0);


	QuImmedieteModeDrawer mDrawer;

	mDrawer.setStyle(GL_TRIANGLE_FAN);
	mDrawer.setLineWidth(1);
	mDrawer.begin();
	mDrawer.color4f(color.toQuColor());
	for(int i = 0; i <= sides+1; i++)
		mDrawer.vertex3f(pts[(i)%(sides)].x, pts[(i)%(sides)].y);
	mDrawer.end();

	color.a += 48;
	color.g = 170;
	color.r = 170;

	mDrawer.setLineWidth(2);
	mDrawer.setStyle(GL_LINES);
	mDrawer.begin();
	mDrawer.color4f(color.toQuColor());
	for(int i = 0; i < sides; i++)
	{
		if(drawSides[i])
		{
			mDrawer.vertex3f(pts[i].x,pts[i].y);
			mDrawer.vertex3f(pts[(i+1)%(sides)].x, pts[(i+1)%(sides)].y);
		}
	}
	mDrawer.end();

	color.a += 32;
	//myStyle.color.g = 255;
	//myStyle.color.r = 255;
	mDrawer.setPointWidth(3);
	mDrawer.setStyle(GL_POINTS);
	mDrawer.begin();
	mDrawer.color4f(color.toQuColor());
	for(int i = 0; i < sides; i++)
		mDrawer.vertex3f(pts[(i)%(sides)].x, pts[(i)%(sides)].y,2);
	mDrawer.end();
	
	glPopMatrix();
}

void drawGuy(float _x, float _y, float _iter)
{
	//_iter *= 5;
	float phi = (_iter+100)/10.0;
	float theta = _iter/101.0;	//so it is relatively prime with 100.0
	float tau = _iter/32.0;
	//get a point of the sphere and convert it to xyz
	QuVector3<float> pt;
	pt.z = cos(theta);
	pt.x = pt.y = sin(theta);
	pt.y *= sin(phi);
	pt.x *= cos(phi);
	float bright = 80 * sin(tau);
	pt *= bright;

	Qu32BitColor color;
	color.r = color.b = color.g = 180-bright;
	color.r += pt.x;
	color.g += pt.y;
	color.b += pt.z;
	color.r = 255 - color.r;
	color.g = 255 - color.g;
	color.b = 255 - color.b;

	QuImmedieteModeDrawer mDrawer;
	mDrawer.setStyle(GL_POINTS);
	mDrawer.setLineWidth(1);
	mDrawer.begin();
	mDrawer.color4f(color.toQuColor());

	glPushMatrix();
	glTranslatef(_x,_y,0);
	glRotatef(45,0,0,1);

	float steps = 15;
	for(int i = 0; i < steps; i++)
	{
		color.a = i*255.0/steps;

		mDrawer.begin();
		mDrawer.color4f(color.toQuColor());
		mDrawer.setPointWidth(steps-i);
		mDrawer.vertex3f(0,0);
		mDrawer.end();
		//ofRect(-(steps-i)/2.0,-(steps-i)/2.0,(steps-i),(steps-i));
		//ofCircle(0,0,(steps-i));
	}
	
	glPopMatrix();
	
}

template<class T>
struct MaxTracker
{
    bool bInit;
    T val;

    MaxTracker():bInit(false){}
    MaxTracker(T t):bInit(true), val(t){}

    void Boom(T t)
    {
        if(!bInit || t > val)
        {
            bInit = true;
            val = t;
        }
    }
};

void drawPentagonPulse(bool isFlipped, float _x, float _y,  DrawingParameters dp, int nFrame, vector<bool> drawSides, float radius, bool bExit, int sides)
{
	float pulseSpeed = 1/3.0; //1 iteration per 100 cycles;
	float startingMaxHeight = 255 - 30;
	float heightChangeSlope = 3;
	//float heightChangeSlope = 1./10;
	float a = 30;
    MaxTracker<float> mt(0);
    for(list<ffShapeDrawerPair>::iterator itr = dp.pulsePairs.begin(), etr = dp.pulsePairs.end();
        itr != etr; ++itr)
	{
		//calculate the height
		//.x is frames elapsed since pulse triggered
		float h = startingMaxHeight - (nFrame - itr->x) * heightChangeSlope;
		//float h = startingMaxHeight*(1./((nFrame - itr->x) * heightChangeSlope + 1) );
        if(h < startingMaxHeight/8)
            h = startingMaxHeight/8;
		//calculate the distance from pulse
		//.y is iteration from pulse triggering source
		float d = abs(itr->y - pulseSpeed*(nFrame - itr->x))/2.0;

		//calculate the alpha
		mt.Boom(h*exp(-(d*d)));
        //if(pulsePairs[i].y == 0)
        //    a = startingMaxHeight;
	}
    if(bWaveLight)
        a += mt.val;
    if(bPathLight)
    {
        int nPathLight = 255 / 2 * (1 - float(dp.nPath) / 7);
        if(nPathLight > 0)
            a += nPathLight;
    }
    if(bReachLight && dp.nAge == 0)
        a += 150;
    if(bReachLight && dp.nAge == 1)
        a += 100;

    if(bHighlight)
        a += 200;

    a = a * dp.fFade;

	if(drawSides.size() != 5 || bHighlight)
		drawSides = vector<bool> (5, true);

    double offset = 0;
	if(isFlipped)
		offset = PI;	//should be pi

	//determine pentagon points
	std::vector<ffShapeDrawerPair> pts = std::vector<ffShapeDrawerPair>(sides);
	for(int i = 0; i < sides; i++)
		pts[i] = ffShapeDrawerPair(radius*sin(offset+(PI*2*i/(float)sides)),radius*cos(offset+(PI*2*i/(float)sides)));

	glPushMatrix();
	glTranslatef(_x,_y,0);


	QuImmedieteModeDrawer mDrawer;
	mDrawer.setStyle(GL_TRIANGLE_FAN);
	Qu32BitColor color;
	color.a = a;
	color.r = color.g = color.b = 210;

    mDrawer.begin();
	mDrawer.color4f(color.toQuColor());
	for(int i = 0; i <= sides+1; i++)
		mDrawer.vertex3f(pts[(i)%(sides)].x, pts[(i)%(sides)].y);
	mDrawer.end();

	color.r = 240;
	color.g = 200;
	color.b = 120;
	mDrawer.setStyle(GL_LINES);
	mDrawer.setLineWidth(3);

	mDrawer.begin();
	mDrawer.color4f(color.toQuColor());
	for(int i = 0; i < sides; i++)
	{
		if(drawSides[i])
		{
			mDrawer.vertex3f(pts[i].x,pts[i].y);
			mDrawer.vertex3f(pts[(i+1)%(sides)].x, pts[(i+1)%(sides)].y);
		}
	}
	mDrawer.end();

	color.r = color.g = color.b = 255;

	mDrawer.setPointWidth(2);
	mDrawer.setStyle(GL_POINTS);
	mDrawer.begin();
	mDrawer.color4f(color.toQuColor());
	for(int i = 0; i < sides; i++)
		mDrawer.vertex3f(pts[(i)%(sides)].x, pts[(i)%(sides)].y,2);
	mDrawer.end();

	if(bExit)
	{
		mDrawer.setStyle(GL_TRIANGLE_FAN);
		glDisable(GL_BLEND);
		color.a = 255; // should not need this but just in case
		color.r = color.g = color.b = 255;

		
		mDrawer.begin();
		mDrawer.color4f(color.toQuColor());
		mDrawer.vertex3f(-radius/4,radius/2);
		mDrawer.vertex3f(-radius/4.+radius/2.,radius/2.);
		mDrawer.vertex3f(-radius/4.+radius/2.,radius/2.-radius);
		mDrawer.vertex3f(-radius/4.,radius/2.-radius);
		mDrawer.end();


		color.r = color.g = color.b = 140;
		//draw the door

		mDrawer.begin();
		mDrawer.color4f(color.toQuColor());
		mDrawer.vertex3f(-radius/4.,radius/2.);
		mDrawer.vertex3f(-radius/4.+radius/2.-radius/20.,radius/2.+radius/10.);
		mDrawer.vertex3f(-radius/4.+radius/2.-radius/20.,radius/2.-radius+radius/10.);
		mDrawer.vertex3f(-radius/4.,radius/2.-radius);
		mDrawer.end();
		glEnable(GL_BLEND);
	}    
    
    glPopMatrix();
}

void drawLoadingScreen()
{
	QuBasicCamera camera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT);
	//camera.setRotation(G_DR_90);
	camera.setRotation(FOLD_SCREEN_ROTATION);
	QuImageDrawObject d;

	d.setImage(G_IMAGE_MANAGER.getFlyImage("loading.png"));

	glPushMatrix();
	camera.setScene();

	//TODO get rid of me and use your qucontext class
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(false);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	float dim = quMin(camera.getWidth(),camera.getHeight());
	glScalef(dim,dim,1);
	d.autoDraw();
	glPopMatrix();
}

void drawMenuBackground()
{
	QuImageDrawObject d;
	QuStupidPointer<QuBaseImage> img = G_IMAGE_MANAGER.getFlyImage("menubg.png");
	d.setImage(img);

	glPushMatrix();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(false);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	glScalef(img->getWidth(),img->getHeight(),1);
	d.autoDraw();
	glPopMatrix();
}

void refreshScreen()
{
	IwGLSwapBuffers();
}
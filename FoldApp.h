#pragma once
#include "PenMaze.h"
#include "QuGameManager.h"
#include "QuClutils.h"
#include "QuGlobals.h"
//TODO redo the enums here
#include "s3eKeyboard.h"
#include "s3eAudio.h"
#include "QuButton.h"
#include "QuMouse.h"


class FoldApp
{
    private:
		//PentagonLinearMenu* pMenu;
        Game* pGm;
        PentaMaze* pPmz;

        bool bRight;
        bool bLeft;
        bool bUp;
        bool bDown;

        float fZoom;

	public:

		fPoint mMoveDir;
		QuAbsScreenIntBoxButton but;


		//does not get called until context is destroyed, careful with this guy
        ~FoldApp();

		void setup();
		void update();
		void draw();
		void terminate(){}

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

};


class FiveFoldManager : public QuBaseManager
{
	bool mInit;
	QuStupidPointer<FoldApp> g;
	bool mMouseDown;
	QuScreenCoord mPrevCrd;
	QuTimer mLastPulse;
	QuTimer mClickDown;
	bool wasOnRestart;
	QuMultiTouchManager mManager;
	float mPinchChangeDistance;
public:
	FiveFoldManager():mInit(false),mPrevCrd(0,0,0,0),mLastPulse(0,30),mClickDown(0,5),wasOnRestart(false),mPinchChangeDistance(0)
	{
		srand(time(NULL));
		mMouseDown = false;
		mLastPulse.expire();
	}
	virtual ~FiveFoldManager()
	{
		g.setNull();
	}
	virtual void initialize()
	{
		glClearColor(0.5f,0.5f,0.5f,1.0f);
			//set our state
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		g = QuStupidPointer<FoldApp>(new FoldApp());
		g->setup();
		mInit = true;

		s3eAudioPlay("cube_beat.mp3",0);
		G_SOUND_MANAGER.loadSound("pulse.raw");
		G_SOUND_MANAGER.loadSound("win.raw");
	}
	virtual void update()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		mLastPulse.update();
		mClickDown.update();
		g->update();
		g->draw();
	}
	virtual void keyDown(int key){}
	virtual void keyUp(int key){}

	virtual void multiTouch(int button, bool state,  QuScreenCoord crd)
	{
		mManager.multiTouch(button,state,crd);
		if(state == false && button <3)
			mPinchChangeDistance = 0;
	}
	virtual void multiMotion(int button,  QuScreenCoord crd)
	{
		mManager.multiMotion(button,crd);
		float pinch = mManager.getFPinch();
		std::cout << pinch << std::endl;
		if(quAbs(pinch-mPinchChangeDistance) > 0.05f)
		{
			if(pinch-mPinchChangeDistance > 0)
				g->keyPressed('a');
			else
				g->keyPressed('s');
			mPinchChangeDistance = pinch;
		}
	}

	virtual void singleDown(int button, QuScreenCoord crd)
	{
		mMouseDown = true;
		mPrevCrd = crd;
		mClickDown.reset();
		if(g->but.click(crd,FOLD_SCREEN_ROTATION))
			wasOnRestart = true;

		//hack
		singleMotion(QuScreenCoord(),crd);
	}
	virtual void singleUp(int button, QuScreenCoord crd)
	{
		//hacky button stuff
		if(g->but.click(crd,FOLD_SCREEN_ROTATION) && wasOnRestart)
		{
			wasOnRestart = true;
			g->keyPressed(FFEXIT);
		}
		wasOnRestart = false;

		mMouseDown = false;
		QuVector2<int> c = crd.getIChange(mPrevCrd);
		if(!mClickDown.isExpired() && mLastPulse.isExpired() && c.x + c.y <= 5)
		{
			mLastPulse.reset();
			g->keyPressed(FFSPACE);
			G_SOUND_MANAGER.playSound("pulse.raw");
		}

		g->mMoveDir = fPoint(0,0);
		/*
		g->keyReleased(FFDOWN);
		g->keyReleased(FFRIGHT);
		g->keyReleased(FFUP);
		g->keyReleased(FFLEFT);*/
	}
	virtual void singleMotion(QuScreenCoord prevCrd, QuScreenCoord crd)
	{
		if(mMouseDown)
		{
			crd.rotate(FOLD_SCREEN_ROTATION);
			QuFVector2<float> c = crd.getFPosition();
			
			QuFVector2<float> fc;

			//NOTE this is a total hack fix 
			if(G_DEVICE_ROTATION == G_DR_0 || G_DEVICE_ROTATION == G_DR_180)
				fc = QuFVector2<float>(0.5f-c.x,c.y-0.5f);
			else
				fc = QuFVector2<float>(-(0.5f-c.x),-(c.y-0.5f));

			if(fc.magnitude() > 0.06f)
			{
				
				fc.normalize();
				g->mMoveDir = fPoint(fc.x,fc.y);
				/*
				if(quAbs(c.x) > quAbs(c.y))
				{
					if(c.x > 0)
					{
						g->keyPressed(FFLEFT);
						g->keyReleased(FFRIGHT);
						g->keyReleased(FFUP);
						g->keyReleased(FFDOWN);
					}
					else
					{
						g->keyPressed(FFRIGHT);
						g->keyReleased(FFLEFT);
						g->keyReleased(FFUP);
						g->keyReleased(FFDOWN);
					}
				}
				else
				{
					if(c.y > 0)
					{
						g->keyPressed(FFDOWN);
						g->keyReleased(FFRIGHT);
						g->keyReleased(FFLEFT);
						g->keyReleased(FFUP);
					}
					else
					{
						g->keyPressed(FFUP);
						g->keyReleased(FFRIGHT);
						g->keyReleased(FFDOWN);
						g->keyReleased(FFLEFT);
					}
				}
				*/
			}
			
			else
			{
				
				g->mMoveDir = fPoint(0,0);
				/*
				g->keyReleased(FFDOWN);
				g->keyReleased(FFRIGHT);
				g->keyReleased(FFUP);
				g->keyReleased(FFLEFT);*/
			}

		}
	}
};

